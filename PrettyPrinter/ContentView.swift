//
//  ContentView.swift
//  PrettyPrinter
//
//  Created by Nicolas VERINAUD on 04/09/2020.
//  Copyright © 2020 Ryfacto. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
