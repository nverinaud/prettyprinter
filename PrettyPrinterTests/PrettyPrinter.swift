//
// Created by Nicolas VERINAUD on 04/09/2020.
// Copyright (c) 2020 Ryfacto. All rights reserved.
//

import Foundation

struct ThePrettyPrinter {
  
  static func format(_ data: [[String: CustomStringConvertible]]) -> String {
    if data.isEmpty {
      return ""
    }
    let keys = self.keys(data)
    let headerResult = self.header(keys)
    let values = self.formattedValues(data, for: keys)
    let separator = self.separator(data)
    
    return """
           \(separator)
           \(headerResult)
           \(separator)
           \(values)
           \(separator)
           """
  }
  
  private static func header(_ keys: [String]) -> String {
    "| " + keys.joined(separator: " | ") + " |"
  }
  
  private static func formattedValues(_ data: [[String: CustomStringConvertible]], for keys: [String]) -> String {
    let values = self.values(data, for: keys)
  
    return values.map { line in
      "| " + line.map { $0.description }.joined(separator: " | ") + " |"
    }.joined(separator: "\n")
  }
  
  private static func separator(_ data: [[String: CustomStringConvertible]]) -> String {
    let keys = self.keys(data)
    let values = self.values(data, for: keys)
    let sizeOfFirstKey = keys.first!.count
    let sizeOfFirstValue = values.first!.first!.description.count
    let size = max(sizeOfFirstKey, sizeOfFirstValue)
    return String(repeating: "-", count: size + 4)
  }
  
  private static func keys(_ data: [[String: CustomStringConvertible]]) -> [String] {
    Array(data.first!.keys).sorted(by: { $0 < $1 })
  }
  
  private static func values(_ data: [[String: CustomStringConvertible]], for keys: [String]) -> [[CustomStringConvertible]] {
    data.map { el in
      keys.map { key in
        el[key]!
      }
    }
  }
}
