//
//  Created by Nicolas VERINAUD on 04/09/2020.
//  Copyright © 2020 Ryfacto. All rights reserved.
//

import XCTest
@testable import PrettyPrinter

class PrettyPrinterTests: XCTestCase {
  
  func test_Empty_array_returns_an_empty_string() {
    XCTAssertEqual("", format([]))
  }
  
  func test_Creates_header_according_to_keys() {
    assertThatHeader(is: header("age", "name"), for: headerData("age", "name"))
    assertThatHeader(is: header("ags", "nome"), for: headerData("ags", "nome"))
    assertThatHeader(is: header("a", "b", "c"), for: headerData("a", "b", "c"))
  }
  
  func test_Creates_data_lines_according_to_data_values() {
    assertThatDataLines(are: [ line(22, "Alice") ], for: [ data(22, "Alice") ])
    assertThatDataLines(are: [ line(23, "Bob") ], for: [ data(23, "Bob") ])
    assertThatDataLines(are: [ line(22, "Alice"), line(23, "Bob") ], for: [ data(22, "Alice"), data(23, "Bob") ])
  }
  
  func test_Creates_separators_according_to_data() {
    // | a  |
    // | 1  |
    // | 11 |
    // ------ 6
    
    assertThatSeparatorIsOfSize(5, whenPropertyNameIsOfSize: 1, andValueIsOfSize: 1)
    assertThatSeparatorIsOfSize(6, whenPropertyNameIsOfSize: 2, andValueIsOfSize: 1)
    assertThatSeparatorIsOfSize(6, whenPropertyNameIsOfSize: 1, andValueIsOfSize: 2)
  
    // | a | b |
    // | 1 | 1 |
    // --------- 9
  
    // | a | b  |
    // | 1 | 11 |
    // ---------- 10
  }
  
  private func assertThatSeparatorIsOfSize(_ separatorSize: Int, whenPropertyNameIsOfSize headerSize: Int, andValueIsOfSize valueSize: Int, _ line: UInt = #line) {
    let key = String(repeating: "a", count: headerSize)
    let value = String(repeating: "1", count: valueSize)
    let data = [ key: value ]
    let expected = String(repeating: "-", count: separatorSize)
    assertThatSeparator(is: expected, for: [data], line)
  }
  
  private func assertThatHeader(is expected: String, for data: [String: CustomStringConvertible], _ line: UInt = #line) {
    let actual = extractHeader(format([ data ]))
    XCTAssertEqual(expected, actual, line: line)
  }
  
  private func assertThatDataLines(are expected: [String], for data: [[String: CustomStringConvertible]], _ line: UInt = #line) {
    let actual = removeBottomSeparator(removeHeader(format(data)))
    XCTAssertEqual(expected, actual, line: line)
  }
  
  private func assertThatSeparator(is expected: String, for data: [[String: CustomStringConvertible]], _ line: UInt = #line) {
    let result = format(data)
    let splitted = result.split(separator: "\n").map(String.init)
    let topSeparator = splitted[0]
    let belowHeaderSeparator = splitted[2]
    let bottomSeparator = splitted.last!
    XCTAssertEqual(expected, topSeparator, "Top separator", line: line)
    XCTAssertEqual(expected, belowHeaderSeparator, "Below header separator", line: line)
    XCTAssertEqual(expected, bottomSeparator, "Bottom separator", line: line)
  }
  
  private func removeHeader(_ string: String) -> [String] {
    let x = string.split(separator: "\n").dropFirst(3).map { String($0) }
    return Array(x)
  }
  
  private func removeBottomSeparator(_ strings: [String]) -> [String] {
    strings.dropLast(1)
  }
  
  private func extractHeader(_ string: String) -> String {
    string.split(separator: "\n").map { String($0) }[1]
  }
  
  private func header(_ keys: String...) -> String {
    "| " + keys.joined(separator: " | ") + " |"
  }
  
  private func line(_ age: Int, _ name: String) -> String {
    """
    | \(age) | \(name) |
    """
  }
  
  private func headerData(_ keys: String...) -> [String: CustomStringConvertible] {
    var result: [String: CustomStringConvertible] = [:]
    keys.forEach {
      result[$0] = ""
    }
    return result
  }
  
  private func data(_ age: Int, _ name: String) -> [String: CustomStringConvertible] {
    [
      "age": age,
      "name": name
    ]
  }
  
  private func format(_ data: [[String: CustomStringConvertible]]) -> String {
    ThePrettyPrinter.format(data)
  }
  
  // separator according to column size
  // header format according to column size
  // acceptance test
  
  /*
    let data = [
      {
        "age": 22,
        "name": "Alice"
      },
      {
        "age": 23,
        "name": "Bob"
      }
    ]
  
    ->
        ---------------
        | age | name  |
        ---------------
        | 22  | Alice |
        | 23  | Bob   |
        ---------------
  */
}
